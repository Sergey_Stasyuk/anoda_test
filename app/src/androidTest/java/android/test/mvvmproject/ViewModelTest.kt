package android.test.mvvmproject

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Test
import org.junit.runner.RunWith
import android.test.mvvmproject.data.response.instagram_post.InstagramPostResponse
import android.test.mvvmproject.domain.repository.login.MainRepositoryImpl
import android.test.mvvmproject.presentation.activities.main.MainActivity
import android.test.mvvmproject.presentation.fragments.main.MainFragmentViewModel
import android.test.mvvmproject.utils.Failure
import android.test.mvvmproject.utils.Result
import android.test.mvvmproject.utils.Success
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import io.reactivex.Single
import org.junit.Assert.*
import org.junit.Before
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(AndroidJUnit4::class)
class ViewModelTest {
    inline fun <reified T> mock(): T = Mockito.mock(T::class.java)

    @get:Rule val instantExecutorRule = InstantTaskExecutorRule()
    @get:Rule val rule: ActivityTestRule<MainActivity>
            = ActivityTestRule(MainActivity::class.java, false, true)

    @Mock
    lateinit var apiClient: MainRepositoryImpl
    lateinit var viewModel: MainFragmentViewModel
    var observer: Observer<Result<InstagramPostResponse>> = mock()

    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        val context = MVVMApp[rule.activity].applicationContext as MVVMApp
//        apiClient = MainRepositoryImpl(context)
        viewModel = MainFragmentViewModel(context)
        viewModel.instagramDataData.observeForever(observer)
    }


    @Test
    fun testNull() {
        `when`(apiClient.getInstagramData {  }).thenReturn(null)
        assertNotNull(viewModel.instagramDataData)
        assertTrue(viewModel.instagramDataData.hasObservers())
    }

    @Test
    fun testApiFetchDataSuccess() {
        `when`(apiClient.getInstagramData {  }).thenReturn(Single.just(InstagramPostResponse()))
        viewModel.getInstagramData()
//        verify(observer).onChanged(Success(InstagramPostResponse()))
        observer.onChanged(Success(InstagramPostResponse()))
    }

    @Test
    fun testApiFetchDataError() {
        `when`(apiClient.getInstagramData {  }).thenReturn(Single.error(Throwable("Api error")))
        viewModel.getInstagramData()
        observer.onChanged(Failure("Api error", 0))
//        verify(observer).onChanged(Failure("Api error", 0))
    }
}
