package android.test.mvvmproject

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Test
import org.junit.runner.RunWith
import android.app.Activity
import android.content.Intent
import android.test.mvvmproject.data.response.instagram_post.InstagramPostResponse
import android.test.mvvmproject.domain.repository.login.MainRepositoryImpl
import android.test.mvvmproject.presentation.activities.main.MainActivity
import android.test.mvvmproject.presentation.fragments.main.MainFragmentViewModel
import android.test.mvvmproject.utils.Result
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.lifecycle.ActivityLifecycleCallback
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import androidx.test.runner.lifecycle.Stage
import org.junit.Rule
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.RootMatchers
import androidx.test.espresso.matcher.ViewMatchers

import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.Matchers.*
import org.junit.Assert.*
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.junit.Before
import org.mockito.Mockito.`when`

@RunWith(AndroidJUnit4::class)
class ButtonsTest {

    @get:Rule val rule: ActivityTestRule<MainActivity>
            = ActivityTestRule(MainActivity::class.java, false, true)

    @Test
    fun useAppContext() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("android.test.mvvmproject", appContext.packageName)
    }

    @Test
    fun testMainActivityButtonToastMessages() {
        checkButtonPressedToast(R.id.camera_button, R.string.camera_button_message)
        checkButtonPressedToast(R.id.messaging_button, R.string.messaging_button_message)
        checkButtonPressedToast(R.id.logo_image_view, R.string.logo_message)

        checkButtonPressedToast(R.id.nav_home, R.string.bottom_home_button_message)
        checkButtonPressedToast(R.id.nav_search, R.string.bottom_search_button_message)
        checkButtonPressedToast(R.id.nav_add_post, R.string.bottom_add_post_button_message)
        checkButtonPressedToast(R.id.nav_favourites, R.string.favourites_button_message)
        checkButtonPressedToast(R.id.nav_profile, R.string.bottom_profile_button_message)
    }

    @Test
    fun testMainFragmentButtonToastMessages() {
        checkButtonPressedToast(R.id.profile_small_image_view, R.string.profile_image_message)
        checkButtonPressedToast(R.id.profile_more_image_view, R.string.more_button_message)

        checkButtonPressedToast(R.id.favourites_button, R.string.favourites_button_message)
        checkButtonPressedToast(R.id.chat_button, R.string.chat_button_message)
        checkButtonPressedToast(R.id.messaging_post_button, R.string.messaging_button_message)
        checkButtonPressedToast(R.id.bookmark_button, R.string.bookmark_button_message)
    }

    private fun checkButtonPressedToast(viewId: Int, messageResourceId: Int){
        onView(allOf(withId(viewId), isDisplayed()))
            .perform(click())
        onView(withText(messageResourceId))
            .inRoot(withDecorView(not(`is`(rule.activity.window.decorView))))
            .check(matches(isDisplayed()))
    }

}
