package android.test.mvvmproject.utils

import android.graphics.drawable.Drawable
import android.text.Spanned
import android.view.View
import android.view.ViewTreeObserver
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.core.text.HtmlCompat
import coil.Coil
import coil.api.load
import coil.request.CachePolicy
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target

fun View.visible(){
    this.visibility = View.VISIBLE
}

fun View.invisible(){
    this.visibility = View.INVISIBLE
}

fun View.gone(){
    this.visibility = View.GONE
}

fun Button.enable(){
    this.isEnabled = true
}

fun Button.disable(){
    this.isEnabled = false
}

fun View.listenForDrawn(onViewDrawn:(view: View)->Unit){
    val view = this
    view.viewTreeObserver.addOnGlobalLayoutListener(object: ViewTreeObserver.OnGlobalLayoutListener{
        override fun onGlobalLayout() {
            view.viewTreeObserver.removeOnGlobalLayoutListener(this)
            onViewDrawn(view)
        }
    })
}

fun ImageView.loadImageCoil(url: String, progress: View? = null, onSuccess:(resource: Drawable)->Unit = {}, onError:()->Unit = {}) {
    if (url != "") {
        val imageView = this
        Coil.load(context, url) {
            memoryCachePolicy(CachePolicy.DISABLED)
            target { drawable ->
                onSuccess(drawable)
                imageView.load(drawable)
            }
            listener(onStart = {
                progress?.gone()
            }, onCancel = {
                progress?.gone()
            }, onError = { data, throwable ->
                progress?.gone()
                onError()
            }, onSuccess = { data, source ->
                progress?.gone()
            })
        }
    }
}

fun ImageView.loadImage(url: String, progress: View? = null, onSuccess:(resource: Drawable)->Unit = {}, onError:()->Unit = {}) {
    if (url != "") {
        progress?.visible()
        val options = RequestOptions()
                .encodeQuality(33)
                .centerInside()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
        Glide.with(this.context)
                .load(url)
                .apply(options)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any, target: Target<Drawable>, isFirstResource: Boolean): Boolean {
                        progress?.gone()
                        onError()
                        return false
                    }

                    override fun onResourceReady(resource: Drawable, model: Any, target: Target<Drawable>, dataSource: DataSource, isFirstResource: Boolean): Boolean {
                        progress?.gone()
                        onSuccess(resource)
                        return false
                    }
                })
                .into(this)
    }
}

fun String.fromHtml(): Spanned {
    return HtmlCompat.fromHtml(this, HtmlCompat.FROM_HTML_MODE_LEGACY)
}

