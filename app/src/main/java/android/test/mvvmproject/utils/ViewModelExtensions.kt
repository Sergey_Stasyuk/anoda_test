package android.test.mvvmproject.utils

import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*
import java.lang.Exception

inline fun <reified T : ViewModel> FragmentActivity.injectViewModel(factory: ViewModelProvider.Factory): T {
    return ViewModelProviders.of(this, factory)[T::class.java]
}

inline fun <reified T : ViewModel> Fragment.injectViewModel(factory: ViewModelProvider.Factory): T {
    return ViewModelProviders.of(this, factory)[T::class.java]
}

fun <T> LiveData<T>.observe(@NonNull owner: LifecycleOwner, onUpdate: (t: T) -> Unit) {
    this.observe(owner, Observer { data ->
        data?.let { onUpdate(data) }
    })
}

sealed class Result<T>
data class Success<T>(val value: T) : Result<T>()
data class Failure<T>(val errorMessage: String, val errorCode: Int) : Result<T>()
data class Loading<T>(val isLoading: Boolean) : Result<T>()