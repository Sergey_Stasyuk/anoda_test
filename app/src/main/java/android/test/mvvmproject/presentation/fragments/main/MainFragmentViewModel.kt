package android.test.mvvmproject.presentation.fragments.main

import android.app.Application
import android.test.mvvmproject.MVVMApp
import android.test.mvvmproject.R
import android.test.mvvmproject.data.response.instagram_post.InstagramPostResponse
import android.test.mvvmproject.mvvm.BaseViewModel
import android.test.mvvmproject.utils.Result
import android.test.mvvmproject.utils.Success
import android.test.mvvmproject.utils.inputStreamToString
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import javax.inject.Inject

class MainFragmentViewModel @Inject constructor(app: Application): BaseViewModel(app) {

    val instagramDataData = MutableLiveData<Result<ArrayList<InstagramPostResponse>>>()

    fun getInstagramData(){
        mainApi.getInstagramData {
            instagramDataData.postValue(it)
        }.call()
    }
}