package android.test.mvvmproject.presentation.listeners

import android.view.View

interface MessageClickListener {
    fun getClickListener(
        view: View, durationMillis: Long, toastText: String,
        toastRes: Int, onClick: (view: View) -> Unit
    )

    companion object {
        val empty = object : MessageClickListener {
            override fun getClickListener(
                view: View,
                durationMillis: Long,
                toastText: String,
                toastRes: Int,
                onClick: (view: View) -> Unit
            ) {
            }
        }
    }
}