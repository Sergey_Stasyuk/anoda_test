package android.test.mvvmproject.presentation.adapters.view_pager

import android.content.Context
import android.test.mvvmproject.R
import android.test.mvvmproject.utils.loadImage
import android.util.Log
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import androidx.viewpager.widget.PagerAdapter
import kotlinx.android.synthetic.main.view_pager_image.view.*
import com.bumptech.glide.Glide

class ImageViewPagerAdapter(val context: Context,
                     private val imageLinksList: ArrayList<String>): PagerAdapter() {

    private val TAG = "GalleryAdapter"

    override fun instantiateItem(collection: ViewGroup, position: Int): Any {
        val currentImageLink = imageLinksList[position]
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.view_pager_image, collection, false) as ViewGroup

        if (currentImageLink.isNotEmpty()) {
            layout.loading_spinner.visibility = View.VISIBLE

            layout.view_pager_image.loadImage(currentImageLink, layout.loading_spinner)
        }

        collection.addView(layout)
        return layout
    }

    override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
        // Removes the page from the container for the given position. We simply removed object using removeView()
        // but could’ve also used removeViewAt() by passing it the position.
        try {
            // Remove the view from the container
            container.removeView(view as View)

            // Try to clear resources used for displaying this view
            Glide.with(container.context).clear(view.findViewById<View>(R.id.view_pager_image))
            // Remove any resources used by this view
            unbindDrawables(view)
        } catch (e: Exception) {
            Log.w(TAG, "destroyItem: failed to destroy item and clear it's used resources", e)
        }

    }

    /**
     * Recursively unbind any resources from the provided view. This method will clear the resources of all the
     * children of the view before invalidating the provided view itself.
     *
     * @param view
     * The view for which to unbind resource.
     */
    protected fun unbindDrawables(view: View) {
        if (view.background != null) {
            view.background.callback = null
        }
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                unbindDrawables(view.getChildAt(i))
            }
            view.removeAllViews()
        }
    }

//    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
//        collection.removeView(view as View)
//    }

    override fun getCount(): Int {
        return imageLinksList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }
}