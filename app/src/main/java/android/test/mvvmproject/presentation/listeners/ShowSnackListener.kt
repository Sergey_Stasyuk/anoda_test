package android.test.mvvmproject.presentation.listeners

interface ShowSnackListener {
    fun showErrorSnack(message: String)
    fun showSuccessSnack(message: String)
    companion object {
        val empty = object: ShowSnackListener {
            override fun showErrorSnack(message: String) {}
            override fun showSuccessSnack(message: String) {}
        }
    }
}