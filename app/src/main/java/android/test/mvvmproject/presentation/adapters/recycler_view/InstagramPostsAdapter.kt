package android.test.mvvmproject.presentation.adapters.recycler_view

import android.test.mvvmproject.R
import android.test.mvvmproject.data.ui_models.InstagramPostModel
import android.test.mvvmproject.presentation.adapters.view_pager.ImageViewPagerAdapter
import android.test.mvvmproject.presentation.listeners.MessageClickListener
import android.test.mvvmproject.utils.fromHtml
import android.test.mvvmproject.utils.loadImage
import android.view.View
import kotlinx.android.synthetic.main.item_instagram_posts.*
import java.lang.Exception

class InstagramPostsAdapter(list: ArrayList<InstagramPostModel>, clickListener: MessageClickListener) :
        BaseAdapter<InstagramPostModel, InstagramPostsAdapter.ViewHolder>(list), MessageClickListener by clickListener {

    override fun createViewHolder(view: View, layoutId: Int) = ViewHolder(view)

    override fun getItemViewType(position: Int) = R.layout.item_instagram_posts

    inner class ViewHolder(view: View) : BaseViewHolder(view) {

        init {
            try {

            } catch (e: Exception) {
            }
        }

        override fun bind(pos: Int) {
            initClicks()
            initViews(list[pos])
        }

        private fun initClicks() {
            profile_small_image_view.getClick(toastRes = R.string.profile_image_message)
            profile_more_image_view.getClick(toastRes = R.string.more_button_message)

            favourites_button.getClick(toastRes = R.string.favourites_button_message)
            chat_button.getClick(toastRes = R.string.chat_button_message)
            messaging_post_button.getClick(toastRes = R.string.messaging_button_message)
            bookmark_button.getClick(toastRes = R.string.bookmark_button_message)
        }

        private fun initViews(data: InstagramPostModel) {
            profile_small_image_view.loadImage(data.avatarUrl)
            top_user_name_text_view.text = data.authorName
            user_place_text_view.text = data.postPlace

            liked_people_text_view.text = data.likesMessage.fromHtml()
            hash_tags_text_view.text = data.hashTagsMessage.fromHtml()
            post_time_text_view.text = data.postTime

            initViewPager(data)
        }

        private fun initViewPager(data: InstagramPostModel) {
            if (image_view_pager.adapter == null)
                image_view_pager.adapter = ImageViewPagerAdapter(
                    image_view_pager.context, data.postImages
                )
        }
    }

}