package android.test.mvvmproject.presentation.fragments

import android.content.Context
import android.os.Bundle
import android.test.mvvmproject.R
import android.test.mvvmproject.mvvm.BaseViewModel
import android.test.mvvmproject.mvvm.cicerone.BackButtonListener
import android.test.mvvmproject.mvvm.cicerone.CiceroneFragmentManager
import android.test.mvvmproject.presentation.listeners.ShowToastListener
import android.test.mvvmproject.presentation.activities.BaseActivity
import android.test.mvvmproject.presentation.listeners.MessageClickListener
import android.test.mvvmproject.presentation.listeners.OnUserAvatarLoadedListener
import android.test.mvvmproject.presentation.listeners.ShowSnackListener
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.dialog_progress.*
import javax.inject.Inject

abstract class BaseFragment <V : BaseViewModel>: Fragment(), BackButtonListener {

    protected var onUserAvatarLoadedListener = OnUserAvatarLoadedListener.empty
    protected var showToastListener = ShowToastListener.empty
    protected var showSnackListener = ShowSnackListener.empty
    protected var clickListener = MessageClickListener.empty

    protected lateinit var viewModel: V

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    protected lateinit var baseContext: Context

    @Inject
    lateinit var baseActivity: BaseActivity<*>

    @Inject
    lateinit var compositeDisposable: CompositeDisposable

    @Inject
    lateinit var fragmentManager: CiceroneFragmentManager

    protected var rootView: View? = null

    @LayoutRes
    protected abstract fun layout(): Int
    protected abstract fun initialization(view: View, isFirstInit: Boolean)
    abstract fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): V

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is BaseActivity<*>)
            context.activityComponent.inject(this as BaseFragment<BaseViewModel>)
        if (context is ShowToastListener)
            showToastListener = context
        if (context is ShowSnackListener)
            showSnackListener = context
        if (context is OnUserAvatarLoadedListener)
            onUserAvatarLoadedListener = context
        if (context is MessageClickListener)
            clickListener = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = provideViewModel(viewModelFactory)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = if (layout() != 0)
            addProgressView(inflater.inflate(layout(), container, false))
        else
            super.onCreateView(inflater, container, savedInstanceState)
        return if (rootView == null) view else rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initialization(view, rootView == null)
        rootView = view
        super.onViewCreated(view, savedInstanceState)
    }

    private fun addProgressView(rootView: View): View {
        val rootLayout = FrameLayout(baseContext)
        val layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER)
        rootLayout.layoutParams = layoutParams
        rootLayout.addView(rootView)

        val progressLayout = baseActivity.layoutInflater.inflate(R.layout.dialog_progress, null)
        rootLayout.addView(progressLayout)
        return rootLayout
    }

    fun showProgress(show: Boolean){
        progress_layout?.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun onDestroy() {
        baseActivity.toggleKeyboard(false)
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onBackPressed(): Boolean {
        fragmentManager.exit()
        return true
    }

    fun View.getClick(durationMillis: Long = 300, toastText: String = "",
                      toastRes: Int = -1, onClick: (view: View) -> Unit = {}){
        clickListener.getClickListener(this, durationMillis, toastText, toastRes, onClick)
    }

    fun showErrorSnack(message: String) {
        showSnackListener.showErrorSnack(message)
    }

    fun showSuccessSnack(message: String) {
        showSnackListener.showSuccessSnack(message)
    }
}