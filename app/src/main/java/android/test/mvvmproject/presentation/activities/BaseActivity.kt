package android.test.mvvmproject.presentation.activities

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.test.mvvmproject.MVVMApp
import android.test.mvvmproject.R
import android.test.mvvmproject.di.components.ActivityComponent
import android.test.mvvmproject.di.modules.ActivityModule
import android.test.mvvmproject.mvvm.BaseViewModel
import android.test.mvvmproject.mvvm.cicerone.BackButtonListener
import android.test.mvvmproject.mvvm.cicerone.CiceroneFragmentManager
import android.test.mvvmproject.presentation.listeners.MessageClickListener
import android.test.mvvmproject.presentation.listeners.ShowSnackListener
import android.test.mvvmproject.presentation.listeners.ShowToastListener
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import java.util.concurrent.TimeUnit
import javax.inject.Inject


abstract class BaseActivity<V : BaseViewModel> : AppCompatActivity(),
    ShowToastListener, ShowSnackListener, MessageClickListener {

    protected lateinit var viewModel: V

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    val activityComponent: ActivityComponent by lazy {
        MVVMApp[this].appComponent.plus(ActivityModule(R.id.container,
                this as BaseActivity<BaseViewModel>))
    }

    @Inject
    lateinit var compositeDisposable: CompositeDisposable

    @Inject
    lateinit var cicerone: Cicerone<Router>

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    @Inject
    lateinit var ciceroneNavigator: SupportAppNavigator

    @Inject
    lateinit var fragmentManager: CiceroneFragmentManager

    @Inject
    lateinit var toast: Toast

    @LayoutRes
    abstract fun layout(): Int

    abstract fun initialization()
    abstract fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): V

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initFragmentComponent()

        viewModel = provideViewModel(viewModelFactory)

        if (layout() != 0) {
            setContentView(layout())
            initialization()
        }
    }

    fun initFragmentComponent() {
        activityComponent.inject(this as BaseActivity<BaseViewModel>)
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(ciceroneNavigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    override fun onBackPressed() {
        val fm = supportFragmentManager
        var fragment: Fragment? = null
        val fragments = fm.fragments
        for (f in fragments) {
            if (f.isVisible) {
                fragment = f
                break
            }
        }

        if (fragment != null
                && fragment is BackButtonListener
                && (fragment as BackButtonListener).onBackPressed()) {
        } else {
            fragmentManager.exit()
        }
    }

    fun toggleKeyboard(show: Boolean) {
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (!show)
            inputMethodManager.hideSoftInputFromWindow(window.decorView.windowToken, 0)
        else
            inputMethodManager.toggleSoftInputFromWindow(window.decorView.windowToken,
                    InputMethodManager.SHOW_FORCED, 0)
    }

    override fun getClickListener(view: View, durationMillis: Long, toastText: String,
                          toastRes: Int, onClick:(view: View) -> Unit){
        RxView.clicks(view)
            .throttleFirst(durationMillis, TimeUnit.MILLISECONDS)
            .subscribe ({ _ ->
                if (toastText.isNotEmpty()) showToast(toastText)
                else if (toastRes >= 0) showToast(getString(toastRes))
                onClick(view)
            }, {
                showErrorSnack(it.message?:getString(R.string.click_operation_error))
            })
            .also { compositeDisposable.add(it) }
    }

    fun View.getClick(durationMillis: Long = 300, toastText: String = "",
                      toastRes: Int = -1, onClick: (view: View) -> Unit = {}){
        getClickListener(this, durationMillis, toastText, toastRes, onClick)
    }

    fun setDarkStatusBar(isDarkTexts: Boolean) {
        if (isDarkTexts && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            window.decorView.systemUiVisibility = window.decorView.systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }

    override fun showToast(message: String) {
        if (::toast.isInitialized && toast.view.windowVisibility == View.VISIBLE)
            toast.cancel()
        toast = Toast.makeText(this, message, Toast.LENGTH_LONG)
        toast.show()
    }

    override fun showErrorSnack(message: String) {
        showSnack(message, R.color.colorRed, Snackbar.LENGTH_LONG)
    }

    override fun showSuccessSnack(message: String) {
        showSnack(message, R.color.colorGreen, Snackbar.LENGTH_LONG)
    }

    private fun showSnack(message: String, colorResource: Int, length: Int) {
        findViewById<View>(R.id.container)?.let { view ->
            Snackbar.make(view, message, length).apply {
                val color = ContextCompat.getColor(view.context, colorResource)
                view.setBackgroundColor(color)
            }.show()
        }
    }
}