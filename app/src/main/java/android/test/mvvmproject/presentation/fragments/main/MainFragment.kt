package android.test.mvvmproject.presentation.fragments.main

import android.test.mvvmproject.R
import android.test.mvvmproject.data.ui_models.InstagramPostModel
import android.test.mvvmproject.presentation.adapters.recycler_view.InstagramPostsAdapter
import android.test.mvvmproject.presentation.fragments.BaseFragment
import android.test.mvvmproject.utils.*
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : BaseFragment<MainFragmentViewModel>() {

    companion object {
        fun getInstance() = MainFragment()
    }

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): MainFragmentViewModel {
        return injectViewModel(viewModelFactory)
    }

    override fun layout() = R.layout.fragment_main

    override fun initialization(view: View, isFirstInit: Boolean) {
        if (isFirstInit) {
            listenUpdates()
            getData()
        }
    }

    private fun initPostsList(list: ArrayList<InstagramPostModel>){
        posts_list.layoutManager = LinearLayoutManager(baseContext).apply { isItemPrefetchEnabled = true }
        posts_list.setHasFixedSize(true)
        posts_list.setItemViewCacheSize(20)
        posts_list.adapter = InstagramPostsAdapter(list, clickListener)
    }

    private fun getData() {
        showProgress(true)
        viewModel.getInstagramData()
    }

    private fun listenUpdates() {
        viewModel.instagramDataData.observe(this) {
            showProgress(false)
            when (it) {
                is Success -> {
                    val data = it.value.flatMap { arrayListOf(it.map(baseContext)) } as ArrayList
                    initPostsList(data)
                    onUserAvatarLoadedListener.onUserAvatarLoaded("https://placeimg.com/500/500/people")
                }
                is Failure -> {
                    showErrorSnack(it.errorMessage)
                }
            }
        }
    }


}