package android.test.mvvmproject.presentation.activities.main

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.test.mvvmproject.R
import android.test.mvvmproject.presentation.activities.BaseActivity
import android.test.mvvmproject.presentation.fragments.main.MainFragment
import android.test.mvvmproject.utils.injectViewModel
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_main.*
import android.test.mvvmproject.presentation.listeners.OnUserAvatarLoadedListener
import android.test.mvvmproject.utils.convertDpToPixel
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition

class MainActivity : BaseActivity<MainActivityViewModel>(),
        OnUserAvatarLoadedListener {

    override fun provideViewModel(viewModelFactory: ViewModelProvider.Factory): MainActivityViewModel {
        return injectViewModel(viewModelFactory)
    }

    override fun layout() = R.layout.activity_main

    override fun initialization() {
        setDarkStatusBar(true)

        initClicks()
        initBottomBar()

        fragmentManager.replaceFragment(MainFragment.getInstance())
    }

    private fun initClicks(){
        camera_button.getClick(toastRes = R.string.camera_button_message)
        messaging_button.getClick(toastRes = R.string.messaging_button_message)
        logo_image_view.getClick(toastRes = R.string.logo_message)
    }

    private fun initBottomBar() {
        nav_home.getClick(toastRes = R.string.bottom_home_button_message) { onBottomBarClickListener(it) }
        nav_search.getClick(toastRes = R.string.bottom_search_button_message) { onBottomBarClickListener(it) }
        nav_add_post.getClick(toastRes = R.string.bottom_add_post_button_message) { onBottomBarClickListener(it) }
        nav_favourites.getClick(toastRes = R.string.favourites_button_message) { onBottomBarClickListener(it) }
        nav_profile.getClick(toastRes = R.string.bottom_profile_button_message) { onBottomBarClickListener(it) }
    }

    private fun onBottomBarClickListener(view: View) {
        when (view.id) {
            R.id.nav_home -> {}
            R.id.nav_search -> {}
            R.id.nav_add_post -> {}
            R.id.nav_favourites -> {}
            R.id.nav_profile -> {}
            else -> {}
        }
    }

    private fun loadBottomBarItemImage(url: String) {
        val size = convertDpToPixel(50f, baseContext).toInt()
        Glide.with(this)
                .asBitmap()
                .load(url)
                .apply(RequestOptions.circleCropTransform().override(size))
                .into(object : CustomTarget<Bitmap>() {
                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                        profile_image_view.setImageBitmap(resource)
                    }
                    override fun onLoadCleared(placeholder: Drawable?) {}
                })
    }

    override fun onUserAvatarLoaded(url: String) {
        loadBottomBarItemImage(url)
    }
}
