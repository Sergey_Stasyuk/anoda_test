package android.test.mvvmproject.presentation.listeners

interface OnUserAvatarLoadedListener {
    fun onUserAvatarLoaded(url: String)
    companion object {
        val empty = object: OnUserAvatarLoadedListener {
            override fun onUserAvatarLoaded(url: String) {}
        }
    }
}