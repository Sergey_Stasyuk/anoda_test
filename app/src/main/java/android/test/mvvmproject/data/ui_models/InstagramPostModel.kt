package android.test.mvvmproject.data.ui_models

data class InstagramPostModel(val authorName: String = "",
                              val avatarUrl: String = "",
                              val postImages: ArrayList<String> = arrayListOf(),
                              val postPlace: String = "",
                              val likesMessage: String = "",
                              val hashTagsMessage: String = "",
                              val postTime: String = "")