package android.test.mvvmproject.data.response.instagram_post

import android.content.Context
import android.test.mvvmproject.R
import android.test.mvvmproject.data.response.BaseResponse
import android.test.mvvmproject.data.ui_models.InstagramPostModel
import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class InstagramPostResponse(

	@field:SerializedName("data")
	val data: Data? = null

): BaseResponse() {

	fun map(context: Context): InstagramPostModel {
		val likesMessage = context.resources.getQuantityString(R.plurals.plural_post_likes,
				data?.likesData?.otherLikes?:0,
                data?.likesData?.likedBy?.joinToString { it },
                data?.likesData?.otherLikes
		)
		val hashTagsMessage =  context.getString(R.string.post_hash_tags,
				data?.userName?:"",
				data?.postType?:"",
				data?.hashTags?.joinToString (separator = " ")
		)
		val postTime = context.getString(R.string.post_time, data?.postTime?:0)
		return InstagramPostModel(
			authorName = data?.userName?:"",
			avatarUrl = data?.avatarUrl?:"",
			postImages = data?.photos?: arrayListOf(),
			postPlace = data?.postPlace?:"",
			likesMessage = likesMessage,
			hashTagsMessage = hashTagsMessage,
			postTime = postTime
		)
	}

}