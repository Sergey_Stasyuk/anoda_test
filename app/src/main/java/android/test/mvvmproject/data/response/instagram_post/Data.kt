package android.test.mvvmproject.data.response.instagram_post

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class Data(

	@field:SerializedName("avatar_url")
	val avatarUrl: String? = null,

	@field:SerializedName("user_name")
	val userName: String? = null,

	@field:SerializedName("post_place")
	val postPlace: String? = null,

	@field:SerializedName("likes_data")
	val likesData: LikesData? = null,

	@field:SerializedName("post_type")
	val postType: String? = null,

	@field:SerializedName("photos")
	val photos: ArrayList<String>? = null,

	@field:SerializedName("hash_tags")
	val hashTags: ArrayList<String>? = null,

	@field:SerializedName("post_time")
	val postTime: Int? = null
)