package android.test.mvvmproject.data.response.instagram_post

import javax.annotation.Generated
import com.google.gson.annotations.SerializedName

@Generated("com.robohorse.robopojogenerator")
data class LikesData(

	@field:SerializedName("other_likes")
	val otherLikes: Int? = null,

	@field:SerializedName("liked_by")
	val likedBy: List<String>? = null
)