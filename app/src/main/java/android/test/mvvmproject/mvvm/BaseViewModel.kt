package android.test.mvvmproject.mvvm

import android.app.Application
import android.test.mvvmproject.MVVMApp
import android.test.mvvmproject.domain.repository.login.MainApiInterface
import android.test.mvvmproject.domain.repository.login.MainRepositoryImpl
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

abstract class BaseViewModel(val app: Application): AndroidViewModel(app) {

    @Inject
    lateinit var compositeDisposable: CompositeDisposable
    @Inject
    lateinit var mainApi: MainRepositoryImpl

    init {
        MVVMApp[app].appComponent.inject(this)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun<T> Single<T>.call(){
        this.subscribe().also { compositeDisposable.add(it) }
    }
}