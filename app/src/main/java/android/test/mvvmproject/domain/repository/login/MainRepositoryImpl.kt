package android.test.mvvmproject.domain.repository.login

import android.content.Context
import android.test.mvvmproject.MVVMApp
import android.test.mvvmproject.R
import android.test.mvvmproject.data.response.instagram_post.InstagramPostResponse
import android.test.mvvmproject.domain.repository.BaseRepository
import android.test.mvvmproject.utils.Failure
import android.test.mvvmproject.utils.inputStreamToString
import android.test.mvvmproject.utils.Result
import android.test.mvvmproject.utils.Success
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Single
import retrofit2.Retrofit
import javax.inject.Inject

open class MainRepositoryImpl @Inject constructor(private val context: Context): BaseRepository() {

//    @Inject
//    lateinit var mainApiInterface: MainApiInterface

    init {
        MVVMApp[context].appComponent.inject(this)
    }

//    open fun getInstagramData(onResponse:(Result<InstagramPostResponse>) -> Unit): Single<InstagramPostResponse> {
//        return Single.fromCallable { parseLocalJsonData() }
//            .getSchedulers()
//            .doOnSuccess {
//                onResponse(Success(it)) }
//            .doOnError {
//                onResponse(Failure(it.message?:context.getString(R.string.error_converting_data), 0)) }
//    }

    open fun getInstagramData(onResponse:(Result<ArrayList<InstagramPostResponse>>) -> Unit): Single<ArrayList<InstagramPostResponse>> {
        return Single.fromCallable { parseLocalJsonData() }
                .getSchedulers()
                .doOnSuccess {
                    onResponse(Success(it)) }
                .doOnError {
                    onResponse(Failure(it.message?:context.getString(R.string.error_converting_data), 0)) }
    }

    fun parseLocalJsonData(): ArrayList<InstagramPostResponse> {
        val myJson = inputStreamToString(context.resources.openRawResource(R.raw.instagram_data))
        return Gson().fromJson(myJson, object : TypeToken<ArrayList<InstagramPostResponse>>() { }.type)
    }
}