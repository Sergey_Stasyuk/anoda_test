package android.test.mvvmproject.domain.repository

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

abstract class BaseRepository {

    fun<T> Single<T>.getSchedulers(): Single<T>{
        return this.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }

}