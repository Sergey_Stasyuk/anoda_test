package android.test.mvvmproject.domain.repository.login

import android.test.mvvmproject.data.response.instagram_post.InstagramPostResponse
import android.test.mvvmproject.utils.Result
import io.reactivex.Single

interface MainApiInterface {
    fun getInstagramData(): Single<InstagramPostResponse>
}