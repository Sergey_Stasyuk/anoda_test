package android.test.mvvmproject.di.modules

import android.content.Context
import android.test.mvvmproject.domain.repository.login.MainRepositoryImpl
import android.test.mvvmproject.domain.repository.login.MainApiInterface
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(includes = [AppModule::class, RetrofitApiModule::class])
class RepositoryModule {

    @Provides
    @Singleton
    fun provideMainRepository(context: Context): MainRepositoryImpl =
        MainRepositoryImpl(context)

//    @Provides
//    @Singleton
//    fun provideMainApi(retrofit: Retrofit): MainApiInterface =
//        retrofit.create(MainApiInterface::class.java)
}