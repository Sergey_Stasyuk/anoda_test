package android.test.mvvmproject.di.modules

import android.test.mvvmproject.di.map_key.ViewModelKey
import android.test.mvvmproject.di.scopes.ActivityScope
import android.test.mvvmproject.presentation.activities.main.MainActivityViewModel
import android.test.mvvmproject.presentation.fragments.main.MainFragmentViewModel
import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @ActivityScope
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    abstract fun bindMainActivityViewModel(model: MainActivityViewModel): ViewModel

    @Binds
    @ActivityScope
    @IntoMap
    @ViewModelKey(MainFragmentViewModel::class)
    abstract fun bindMainFragmentViewModel(model: MainFragmentViewModel): ViewModel
}