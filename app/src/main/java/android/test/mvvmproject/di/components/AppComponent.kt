package android.test.mvvmproject.di.components

import android.app.Application
import android.test.mvvmproject.domain.repository.login.MainRepositoryImpl
import android.test.mvvmproject.di.modules.*
import android.test.mvvmproject.mvvm.BaseViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    RetrofitApiModule::class,
    ViewModelFactoryModule::class,
    RepositoryModule::class,
    NavigationModule::class
])
interface AppComponent {
    fun plus(activityModule: ActivityModule): ActivityComponent

    fun inject(app: Application)
    fun inject(target: BaseViewModel)
    fun inject(target: MainRepositoryImpl)
}