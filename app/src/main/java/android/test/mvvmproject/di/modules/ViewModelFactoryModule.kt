package android.test.mvvmproject.di.modules

import android.test.mvvmproject.di.scopes.ActivityScope
import android.test.mvvmproject.mvvm.DaggerViewModelFactory
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelFactoryModule {
    @Binds
    @ActivityScope
    abstract fun bindViewModelFactory(viewModelFactory: DaggerViewModelFactory): ViewModelProvider.Factory
}