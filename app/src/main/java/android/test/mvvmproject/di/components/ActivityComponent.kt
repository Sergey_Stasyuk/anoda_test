package android.test.mvvmproject.di.components

import android.test.mvvmproject.di.modules.ActivityModule
import android.test.mvvmproject.di.modules.ViewModelModule
import android.test.mvvmproject.di.scopes.ActivityScope
import android.test.mvvmproject.mvvm.BaseViewModel
import android.test.mvvmproject.presentation.activities.BaseActivity
import android.test.mvvmproject.presentation.activities.main.MainActivityViewModel
import android.test.mvvmproject.presentation.fragments.BaseFragment
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [
    ActivityModule::class,
    ViewModelModule::class
])
interface ActivityComponent {
    fun inject(target: BaseActivity<BaseViewModel>)
    fun inject(target: BaseFragment<BaseViewModel>)
}