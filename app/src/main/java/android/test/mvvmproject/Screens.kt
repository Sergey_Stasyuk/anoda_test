package android.test.mvvmproject

import android.test.mvvmproject.mvvm.BaseViewModel
import android.test.mvvmproject.presentation.fragments.BaseFragment
import androidx.fragment.app.Fragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {
    class FragmentScreen<P: BaseViewModel, T: BaseFragment<P>>(var fragment : T): SupportAppScreen() {
        override fun getFragment(): Fragment {
            return fragment
        }
    }
}